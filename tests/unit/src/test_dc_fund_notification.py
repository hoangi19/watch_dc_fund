from dcFundNotification.index import handler
from dcFundNotification.index import LambdaDynamoDBClass, LambdaSNSClass
from unittest import TestCase
from unittest.mock import MagicMock, patch
import os
from boto3 import resource, client
import moto
import json

@moto.mock_dynamodb
@moto.mock_sns
class TestDCFundNotificationLambda(TestCase):

    def setUp(self) -> None:
        self.test_ddb_table_name = 'test_dc_fund'
        self.test_sns_topic_name = 'test_sns_dc_fund'
        os.environ['DYNAMODB_TABLE_NAME'] = self.test_ddb_table_name
        # os.environ['SNS_ARN'] = self.test_sns_topic_name

        dynamodb = resource('dynamodb', region_name='ap-southeast-1')
        sns = client('sns', region_name='ap-southeast-1')
        
        dynamodb.create_table(
                    TableName=self.test_ddb_table_name,
                    KeySchema=[
                        {
                            'AttributeName': 'code',
                            'KeyType': 'HASH'
                        }
                    ],
                    AttributeDefinitions=[
                        {
                            'AttributeName': 'code',
                            'AttributeType': 'S'
                        }],
                    BillingMode='PAY_PER_REQUEST'
        )
        test_items = self.load_test_data("dynamodb_data/test_ccq.json")
        res = dynamodb.Table(self.test_ddb_table_name).put_item(
            Item=test_items
        )

        self.test_topic_arn = sns.create_topic(Name=self.test_sns_topic_name)['TopicArn']
        os.environ['SNS_ARN'] = self.test_topic_arn
        self.mocked_dynamodb = {
            'client': client('dynamodb'),
            'table_name': self.test_ddb_table_name
        }
        self.mocked_sns = {
            'client': client('sns'),
            'sns_arn': self.test_topic_arn
        }

    def load_test_data(self, test_file_path):
        file_path = os.path.join("tests", test_file_path)
        with open(file_path) as f:
            return json.load(f)

    def test_add_item_to_ddb_table(self):
        # dynamodb = resource('dynamodb', region_name='ap-southeast-1')
        return_items = self.mocked_dynamodb['client'].scan(TableName=self.test_ddb_table_name)['Items']
        expected_items = self.load_test_data("dynamodb_data/test_ccq_result.json")

        self.assertEqual(return_items, expected_items)


    @patch('dcFundNotification.index.LambdaSNSClass')
    @patch('dcFundNotification.index.LambdaDynamoDBClass')
    def test_notification(self,
                        mock_dynamodb,
                        mock_sns):
        mock_dynamodb.return_value = LambdaDynamoDBClass(self.mocked_dynamodb)
        mock_sns.return_value = LambdaSNSClass(self.mocked_sns)
        return_value_200 =  {
            'statusCode': 200,
            'body': 'Email sent successfully.'
        }
        test_return_value = handler(None, None)
        self.assertEqual(return_value_200, test_return_value)

    def tearDown(self) -> None:
        dynamodb = resource('dynamodb', region_name='ap-southeast-1')
        dynamodb.Table(self.test_ddb_table_name).delete()
        sns = client('sns', region_name='ap-southeast-1')
        sns.delete_topic(TopicArn=self.test_topic_arn)