import traceback
import boto3
from boto3.dynamodb.types import TypeSerializer, TypeDeserializer
import json

def handler(event,context):
    try:
        code = event['queryStringParameters']['code']
        client = boto3.client('dynamodb')
        response = client.get_item(
            TableName="dc_fund",
            Key={
                "code": {"S": code}
            }
        )
        result = {
            code: response['Item']['info']['M']['nav']['N']
        }
        print(result)
        return {
            'body': json.dumps(result),
            'headers': {
            'Content-Type': 'application/json'
            },
            'statusCode': 200
        }
        
    except Exception:
        traceback.print_exc()