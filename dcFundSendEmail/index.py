import json
import boto3
import os

ses = boto3.client('ses')
MAIL_NAME = os.getenv("MAIL_NAME")
MAIL_SOURCE = os.getenv("MAIL_SOURCE")

def lambda_handler(event, context):
    # TODO implement
    records = event["Records"]
    for record in records:
        body = record['body']
        email_data = json.loads(body)
        print(email_data)
        resp = ses.send_templated_email(
            Source=f'{MAIL_NAME} <{MAIL_SOURCE}>',
            Destination={
                'ToAddresses': [email_data['email']]
            },
            Template='daily_report_mail',
            TemplateData=body
        )
        print(resp)
        
    return {
        'statusCode': 200,
        'body': json.dumps('Hello from Lambda!')
    }
