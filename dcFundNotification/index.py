import boto3
import os
from utils.lambda_dynamodb import LambdaDynamoDBClass, deserializer
from utils.lambda_sns import LambdaSNSClass

_LAMBDA_DYNAMODB_RESOURCE = {
    'client': boto3.client('dynamodb'),
    'table_name': os.getenv('DYNAMODB_TABLE_NAME', 'dc_fund')
}

_LAMBDA_SNS_RESOURCE = {
    'client': boto3.client('sns'),
    'sns_arn': os.getenv("SNS_ARN")
}

def handler(event, context):
    lambda_dynamodb = LambdaDynamoDBClass(_LAMBDA_DYNAMODB_RESOURCE)
    lambda_sns = LambdaSNSClass(_LAMBDA_SNS_RESOURCE)
    items = lambda_dynamodb.client.scan(
            TableName=lambda_dynamodb.table_name
        )

    ccqs = [ deserializer(item) for item in items['Items'] ]
    result = f"Daily report:\n"
    sum_value = 0
    for ccq in ccqs:
        value = ccq['info']['vol']*ccq['info']['nav']
        sum_value += value
        result += f"{ccq['code']} have {ccq['info']['vol']} ccq, nav: {ccq['info']['nav']}. Value: {value}\n"
    result += f"All Value: {sum_value}"

    response = lambda_sns.client.publish(
        TopicArn=lambda_sns.sns_arn,
        Subject='Hoangi19 wallet daily report',
        Message=result
    )
    return {
        'statusCode': 200,
        'body': 'Email sent successfully.'
    }
