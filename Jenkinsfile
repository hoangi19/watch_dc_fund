pipeline {
    agent any
    environment {
        gitlabRepo = "https://gitlab.com/hoangi19/watch_dc_fund.git"
        AWS_ACCESS_KEY_ID = credentials('jenkins-aws-secret-key-id')
        AWS_SECRET_ACCESS_KEY = credentials('jenkins-aws-secret-access-key')
        AWS_DEFAULT_REGION = 'ap-southeast-1' // Change to your desired AWS region
        LAMBDA_FUNCTION_PRICE = 'dcFundPrice'
        LAMBDA_FUNCTION_STOCK = 'dcFundStock'
        LAMBDA_FUNCTION_VALUE = 'dcFundValue'
        LAMBDA_FUNCTION_CRAWL = 'dcFundCrawl'
    }
    options {
        gitLabConnection('gitlab-hoangi19')
    }
    stages {
        stage('Fetch Code') {
            steps {
                script{
                    if (env.BRANCH_NAME) {
                        branch_name = env.BRANCH_NAME
                    } else {
                        branch_name = 'main'
                    }
                }
                git branch: branch_name, url: gitlabRepo
                updateGitlabCommitStatus(state: 'running')
            }
        }
        
        stage ('build lambda layer') {
            steps{
                script{
                    if(sh(script:'git diff-tree --no-commit-id --name-only -r HEAD', returnStdout: true).contains("${LAMBDA_FUNCTION_PRICE}/requirements.txt")){
                        build_layer("${LAMBDA_FUNCTION_PRICE}")
                    }
                    if(sh(script:'git diff-tree --no-commit-id --name-only -r HEAD', returnStdout: true).contains("${LAMBDA_FUNCTION_STOCK}/requirements.txt")){
                        build_layer("${LAMBDA_FUNCTION_STOCK}")
                    }
                    if(sh(script:'git diff-tree --no-commit-id --name-only -r HEAD', returnStdout: true).contains("${LAMBDA_FUNCTION_VALUE}/requirements.txt")){
                        build_layer("${LAMBDA_FUNCTION_VALUE}")
                    }
                    if(sh(script:'git diff-tree --no-commit-id --name-only -r HEAD', returnStdout: true).contains("${LAMBDA_FUNCTION_CRAWL}/requirements.txt")){
                        build_layer("${LAMBDA_FUNCTION_CRAWL}")
                    }
                }
            }
        }
        
        stage('Package code'){
            steps {
                /* package LAMBDA_FUNCTION_PRICE */
                sh 'zip -rj $LAMBDA_FUNCTION_PRICE-code.zip ./$LAMBDA_FUNCTION_PRICE/*'
                
                /* package LAMBDA_FUNCTION_STOCK */
                sh 'zip -rj ${LAMBDA_FUNCTION_STOCK}-code.zip ./$LAMBDA_FUNCTION_STOCK/*'
                
                /* package LAMBDA_FUNCTION_VALUE */
                sh 'zip -rj ${LAMBDA_FUNCTION_VALUE}-code.zip ./$LAMBDA_FUNCTION_VALUE/*'
                
                /* package LAMBDA_FUNCTION_CRAWL */
                sh 'zip -rj ${LAMBDA_FUNCTION_CRAWL}-code.zip ./$LAMBDA_FUNCTION_CRAWL/*'
            }
        }
        stage('Deploy') {
            steps {
                // Update the Lambda function code
                sh 'aws lambda update-function-code --function-name $LAMBDA_FUNCTION_PRICE --zip-file fileb://${LAMBDA_FUNCTION_PRICE}-code.zip'
                
                sh 'aws lambda update-function-code --function-name $LAMBDA_FUNCTION_STOCK --zip-file fileb://${LAMBDA_FUNCTION_STOCK}-code.zip'
                
                sh 'aws lambda update-function-code --function-name $LAMBDA_FUNCTION_VALUE --zip-file fileb://${LAMBDA_FUNCTION_VALUE}-code.zip'
                
                sh 'aws lambda update-function-code --function-name $LAMBDA_FUNCTION_CRAWL --zip-file fileb://${LAMBDA_FUNCTION_CRAWL}-code.zip'
            }
        }
        stage('Post Deploy'){
            steps{
                updateGitlabCommitStatus(state: 'success')
                sh 'rm -f ${LAMBDA_FUNCTION_PRICE}-code.zip'
                sh 'rm -f ${LAMBDA_FUNCTION_STOCK}-code.zip'    
                sh 'rm -f ${LAMBDA_FUNCTION_VALUE}-code.zip'
                sh 'rm -f ${LAMBDA_FUNCTION_CRAWL}-code.zip'
            }
        }
    }
}

def build_layer(funcName) {
    echo "pip install --target ./package/${funcName}/python -r ./${funcName}/requirements.txt"
    sh "pip install --target ./package/${funcName}/python -r ./${funcName}/requirements.txt"
    sh "zip -r python.zip ./package/${funcName}/python"
    LayerVersionArn = sh(script: "aws lambda publish-layer-version --layer-name ${funcName}-layer --zip-file fileb://python.zip --query 'LayerVersionArn'", returnStdout: true)
    sh "aws lambda update-function-configuration --function-name ${funcName} --layer ${LayerVersionArn}"
    sh "rm -f python.zip"
    sh "rm -rf ./package/${funcName}/python"
}