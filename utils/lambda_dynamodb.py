from boto3.dynamodb.types import TypeSerializer, TypeDeserializer

deser = TypeDeserializer()
def deserializer(data: dict):
    return {k: deser.deserialize(v) for k, v in data.items()}

serializer = TypeSerializer()
def serializer(data: dict):
    return {k: serializer.serialize(v) for k, v in data.items()}

class LambdaDynamoDBClass:
    
    def __init__(self, lambda_dynamodb: dict):
        self.client = lambda_dynamodb['client']
        self.table_name = lambda_dynamodb['table_name']
