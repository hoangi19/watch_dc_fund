import requests
import boto3
from decimal import Decimal
import traceback


def update_info_attribute(table_name, code_value, info_updates):
    # Initialize the DynamoDB client
    dynamodb = boto3.client('dynamodb')

    # Fetch the existing item from DynamoDB
    response = dynamodb.get_item(
        TableName=table_name,
        Key={'code': {'S': code_value}}
    )

    # Check if the item exists in the table
    if 'Item' in response:
        item = response['Item']
        existing_info = item.get('info', {}).get('M', {})

        # Update the 'info' attribute with the provided updates
        updated_info = {
            'M': {**existing_info, **info_updates}
        }

        # Update expression to set the 'info' attribute to the new value
        update_expression = 'SET info = :val'

        # Expression attribute values to substitute in the update expression
        expression_attribute_values = {
            ':val': updated_info
        }

        try:
            # Update the item in the DynamoDB table
            dynamodb.update_item(
                TableName=table_name,
                Key={'code': {'S': code_value}},
                UpdateExpression=update_expression,
                ExpressionAttributeValues=expression_attribute_values
            )
            print("Update succeeded!")
        except Exception as e:
            print(f"Update failed: {e}")
    else:
        print(f"Item with code '{code_value}' not found in the table.")

def handler(event,context):
    url = 'https://api.dragoncapital.com.vn/nav/getLatestValue.php?trade_code=DCDS'
    response = requests.get(url=url)
    res_json = response.json()
    info_updates = {
        'nav': {'N': res_json['nav_ccq']}
    }
    update_info_attribute('dc_fund', 'dcds', info_updates)
    
    return {
        'body': 'Success',
        'headers': {
        'Content-Type': 'text/plain'
        },
        'statusCode': 200
    } 